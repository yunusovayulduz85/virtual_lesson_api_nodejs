// const express = require('express');
// const app = express();
// app.use(express.json())
// const books=[
//     { id: 1, name:'rich dad poor dad'},
//     { id: 2, name:'good to great'},
//     { id: 3, name:'the war of art'},
// ]

// app.get('/', (req,res) => {
//     res.send('Salom')
// })

// app.get('/api/books',(req,res) => {
//     res.send(books)
// })

// app.post('/api/books',(req,res) => {
//    const book={
//     id:books.length + 1,
//     name:req.body.name
//    }
//    books.push(book)
//    res.status(201).send(book)
// })

// app.get('/api/books/:id',(req,res) => {
//     const book=books.find(b => b.id === parseInt(req.params.id))

//     if(!book){
//         res.status(404).send('Berilgan IDga teng bo\'lgan kitob topilmadi!')
//     }

//     res.send(book)
// })

// const port=process.env.PORT || 5001
// app.listen(port,()=>{
//     console.log(`${port}chi portni eshitishni boshladim...`);
// })

//!Topshiriq
const express=require('express')
const Joi=require('joi')
const app=express();
app.use(express.json())
const categories=[
    {id:1,name:"Dasturlash"},
    {id:2,name:"Ma'lumot omborlari"},
    {id:3,name:"Kompyuter tarmoqlari"}
]
// json formatdagi ma'lumotlarni olib kelish (get) methodi yoziladi
app.get('/api/categories', (req,res)=>{
    res.send(categories)
})
// aynan tanlangan objectni olib kelish (get) methodi yoziladi
app.get('/api/categories/:id',(req,res) => {
    const data=categories.find(b => b.id===parseInt(req.params.id))
    if(!data){
        res.status(404).send("Kiritilgan id dagi data topilmadi!")
    }
    res.send(data)
})
//yangi object qo'shish
    app.post('/api/categories',(req,res) => {
        const {error} = validateCategory(req.body)
        if(error){
           return res.status(400).send(error.details[0].message)
        }
        const category={
            id:categories.length+1,
            name:req.body.name
        }
       
        categories.push(category)
        res.status(201).send(category)
    })
// aynan tanlangan objectni yangilash put methodi yoziladi
    
    app.put('/api/categories/:id',(req,res) => {
        const data = categories.find(b => b.id === parseInt(req.params.id))
        if (!data) {
           return res.status(404).send("Kiritilgan id dagi data topilmadi!")
        }
        const {error} = validateCategory(req.body)
        if(error){
            return res.status(400).send(error.details[0].message)
        }
        data.name=req.body.name
        res.send(data)

    })
// delete methodi 
    app.delete('/api/categories/:id',(req,res) => {
        const data = categories.find(b => b.id === parseInt(req.params.id))
        if (!data) {
            return res.status(404).send("Kiritilgan id dagi data topilmadi!")
        }
        const categoryIndex=categories.indexOf(data)
        categories.splice(categoryIndex,1)
        res.send(data)
    })
// bir xil kodni bitta funksiyaga olamiz
function validateCategory(category){
   const schema = {
    name:Joi.string().required().min(3)
   }
   return Joi.validate(category,schema)
}
// portni eshitish kodi 
const port=process.env.PORT || 5000
app.listen(port,()=>{
    console.log(`${port}chi portni eshitishni boshladim..`);
})